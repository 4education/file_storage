const allowRoles = require("../../../config/system_roles");
const RoleDontHavePermissionError = require("../exceptions/RoleDontHavePermissionError");
const serviceContainer = require("../../../libs/ServiceContainer");
const redislib = require('../../../libs/RedisClient');
const { serviceName } = require('../../../config/server');
const errorsCodes = require("../../core/mappers/ErrorMessageCodes");

let rolesAllowCreateManualy = [allowRoles.ADMIN, allowRoles.SUPER_ADMIN];

const getUserPlan = (user) => {
    return new Promise((resolve,reject) => {
            redislib.client.get(user.cid+'_'+serviceName, (error, result) => {
                if (error) {
                    return reject(new RoleDontHavePermissionError(errorsCodes.PERMISSION.SERVICE_NOT_PAID));
                }
                if(!result)
                {
                    return reject(new RoleDontHavePermissionError(errorsCodes.PERMISSION.SERVICE_NOT_PAID));
                }
                const accountData = JSON.parse(result);
                let curDate = new Date().setHours(0,0,0,0);
                let expiredDate = new Date(accountData.expiredDate).setHours(0,0,0,0);
                if(curDate >= expiredDate)
                {
                    return reject(new RoleDontHavePermissionError(errorsCodes.PERMISSION.SERVICE_NOT_PAID));
                }
                resolve(JSON.parse(result));
            });
    });
};
const checkIsEmployee = (req) => {
    if(req.user && [allowRoles.CUSTOMER, allowRoles.INTERNAL, allowRoles.SUPER_ADMIN, allowRoles.ADMIN, allowRoles.COMPANY_EMPLOYEE, allowRoles.COMPANY_ADMIN].indexOf(req.user.role) === -1)
    {
        throw new RoleDontHavePermissionError(errorsCodes.PERMISSION.NOT_ALLOWED);
    }
};
const getUserStatisticInfo = async (user) => {
    return serviceContainer.getService('internal', 'statistic').getByCid(user.cid)
};

module.exports = {
    "get_*": {service: "testsService", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            //const {offerInfo} = await getUserPlan(req.user);
        }},
    "post_/upload_file": {service: "fileStorageApi", shouldAuthentificate: true, permission: 'ro',
        conditionFunction: async req => {
            checkIsEmployee(req);
        }}
};