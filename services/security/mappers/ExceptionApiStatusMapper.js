
class ExceptionApiStatusMapper
{
    constructor()
    {
        this.map = {
            "UserNotAuthorizeError" : 401,
            "RoleDontHavePermissionError": 403,
            "Error"                 : 500,
            "InternalError" : 500
        }
    }

    getApiStatus(exception)
    {
        if(this.map[exception.constructor.name])
        {
            return this.map[exception.constructor.name];
        }
        return 500;
    }
}

module.exports = new ExceptionApiStatusMapper();