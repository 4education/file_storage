const EventModel = require('./models/EventModel');
const serviceContainer = require("../../libs/ServiceContainer");

class EventService
{
    // constructor()
    // {
    //     this.store = {};
    // }
    //
    // getStore()
    // {
    //     return this.store;
    // }
    //
    // registerEvent(name)
    // {
    //     if(!this.store[name])
    //     {
    //         this.store[name] = [];
    //     }
    // }
    //
    // registerEvents(types)
    // {
    //     for(let i in types)
    //     {
    //         if(types.hasOwnProperty(i))
    //         {
    //             this.registerEvent(types[i]);
    //         }
    //     }
    // }
    //
    // registerHandler(type, handler, priority = -1)
    // {
    //     if(this.store[type])
    //     {
    //         this.store[type].push(this.__getHandlerObject(handler, priority));
    //         this.store[type].sort((aItem, bItem) => {
    //             return aItem.pr-bItem.pr
    //         });
    //     }
    //     console.log('Event '+type+' does not exists');
    // }
    //
    // __getHandlerObject(callback, priority)
    // {
    //     return {
    //         pr: priority,
    //         cl: callback
    //     };
    // }
    //
    __dispatch(obj)
    {
        if ( obj instanceof EventModel )
        {
            obj.setToInProgress();
            let listeners = serviceContainer.getServicesByType('events');

            if(listeners)
            {
                for (let key in listeners) {
                    if(!listeners.hasOwnProperty(key))
                    {
                        continue;
                    }
                    let listener = listeners[key];
                    if(typeof listener.onEventDispatch === "function")
                    {
                        listener.onEventDispatch(obj.getEventStaticData());
                    }
                }
            }
            obj.finish();
            return;

        }
        throw new Error("Dispatch obj should be of instance of EventModel");
    }
    //
    // __createIterator(arrayData)
    // {
    //     return {
    //             [Symbol.iterator]: () => ({
    //             items: arrayData,
    //             status: 'running',
    //             index: -1,
    //             next: function next () {
    //                 this.index++;
    //                 return {
    //                     done: this.status == 'stopped' ? true : this.index >= this.items.length,
    //                     value: this.items[this.index]
    //                 }
    //             },
    //             stop: () => {
    //                 this.status = 'stopped';
    //             }
    //         })
    //     };
    // }

    dispatchEvent(type, data)
    {
        let event = new EventModel(type, data);
        try{
            this.__dispatch(event);
        }catch(exception)
        {
            console.log(exception, exception.stack.split("\n"));
        }
    }
}


module.exports = new EventService();