const EventService = require("../EventService");


class Internal
{
    /**
     * @param {Object} Event        Object of event.
     * @param {String} Event.type   unique event type.
     * @param {Mixed}  Event.data   Some event data.
     */
    dispatch(event)
    {
        EventService.dispatchEvent(event.type, event.data)
    }
}

module.exports = new Internal();