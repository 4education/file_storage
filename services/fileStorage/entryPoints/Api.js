const CoreApi = require("../../core/entryPoints/Api");
const fileStorageLib = require("../../../libs/fileStorage")

class Api extends CoreApi
{
    /**
     * return object collections which described public api routes of service
     */
    getSchema()
    {
        return {
            "post": {
                "/upload_file": {
                    act: this.__getMethod('uploadFile')
                },
            },
        };
    }

    async uploadFile(req, res)
    {
        let fileStorage = new fileStorageLib(this.getPath(req), true);
        let upload = fileStorage.getUpload().any();
        upload(req, res, (err) => {
            console.log(err);
            if (err) {
                return res.status(500).send(
                    { message: "No possible to upload"}
                );
                return
            }
            if(!req.files || !req.files[0])
            {
                return res.status(404).send(
                    { message: "files not exists"}
                );
            }
            res.status(200).send(
                {
                    fileUri: this.__getFileUri(req),
                    filePath: req.files[0].path,
                    filename: req.files[0].filename,
                    fileSize: req.files[0].size
                }
            );
            // Everything went fine
        })
    }

    getPath(req)
    {
        return req.user.role+'/'+req.user.uid
    }

    __getFileUri(req)
    {
        const { headers: { 'x-forwarded-proto': pr, referer } ={} } = req,
            proto = pr ? pr: referer ? referer.split(':')[0] : process.env.NODE_ENV == 'production' ? 'https' : 'http';
        return `${proto}://${req.get('host')}/${req.files[0].path}`;
    }
}

module.exports = Api;
