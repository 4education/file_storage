const {
	NODE_ENV,
	FILE_STORAGE_SERVICE_HOST,
	FILE_STORAGE_SERVICE_PORT,
} = process.env;

module.exports = {
	"serviceName": "survey",
	"protocol": NODE_ENV == 'production' ? 'https://' : 'http://',
    "host": FILE_STORAGE_SERVICE_HOST,
    "port": FILE_STORAGE_SERVICE_PORT,
	"origin": `${FILE_STORAGE_SERVICE_HOST}${FILE_STORAGE_SERVICE_PORT ? `:${FILE_STORAGE_SERVICE_PORT}` : ''}`
};