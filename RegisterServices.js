let serviceContainer = require("./libs/ServiceContainer");
//======================================================================================================================

//router Service
let routerService = require("./services/router/RouterService");
serviceContainer.registerServiceEntryPoints('internal', 'routerService', routerService);
//======================================================================================================================


//event service
let eventPointInternal = require("./services/event/entryPoints/Internal");
serviceContainer.registerServiceEntryPoints('internal', 'eventService',eventPointInternal);
//======================================================================================================================


//content services
 let fileApi = require("./services/fileStorage/entryPoints/Api");
 // let internalApi = require("./services/content/entryPoints/InternalApi");
 // let eventApi = require("./services/content/entryPoints/EventApi");

 serviceContainer.registerServiceEntryPoints('api', 'fileStorageApi', new fileApi());
 // serviceContainer.registerServiceEntryPoints('internal', 'testsService', new internalApi());
 // serviceContainer.registerServiceEntryPoints('events', 'testsService', new eventApi());
//======================================================================================================================

//security service
let oauthInternal = require("./services/oauth/entryPoints/InternalApi");
routerService.setMiddleWareFunction(new oauthInternal().addUserDataToRequest);

let securityService = require("./services/security/entryPoints/Api");
routerService.setMiddleWareFunction(new securityService().processRequest);
//======================================================================================================================

