const redis = require('redis');

module.exports =
    {
        client:null,
        connect(host, port)
        {
            this.client = redis.createClient(port, host);
            return this.client;
        },

        setHandler(eventName, callback)
        {
            this.client.on(eventName, function(...data){
                console.log(eventName, ...data);
                callback(...data);
            });
        }
    };