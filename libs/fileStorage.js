const multer = require("multer");
const mkdirp = require('mkdirp');
const path = require('path');
const mime = require('mime-types');

class FileStorage
{
    constructor(dirName = '', fileNameAutoGen = false)
    {
        try {
            // Configuring appropriate storage
            this.storage = multer.diskStorage({
                // Absolute path
                destination: async (req, file, callback) => {
                    let dir = './uploads/'+dirName;
                    console.log(dir)
                    await mkdirp(dir, { recursive: true });
                    callback(null, dir);
                },
                filename:  (req, file, callback) => {
                    let fileName = fileNameAutoGen === true ? this.__getFileName(file) : file.fieldname || file.originalname;
                    callback(null, fileName);
                }
            });
        } catch (ex) {
            console.log("Error :\n"+ex);
        }
    }

    getUpload()
    {
        return multer({ storage: this.storage });
    }

    __getFileName(file)
    {
        return new Date().getTime().toString()
            + Math.floor(Math.random()*1000000)
            +this.__getFileExtention(file);
    }


    __getFileExtention(file)
    {
        let extension = path.extname( file.originalname || file.fieldname );
        if (!extension)
        {
            if(file.mimetype)
            {
                extension = '.'+mime.extension(file.mimetype);
            }
        }
        return extension;
    }
}

module.exports = FileStorage;