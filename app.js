const express = require('express');
const bodyParser = require('body-parser');

//run service registration;

require("./RegisterServices");

const serviceContainer = require("./libs/ServiceContainer");

const apiRouterService = serviceContainer.getService('internal', 'routerService');

// Configuration 
const server = require('./config/server');

class AppServer
{
    constructor()
    {
        this.express = express();
        this.express.use(bodyParser.urlencoded({ extended: true }));
        this.express.use(bodyParser.json());
    }

    run()
    {
        //register api
        this.setHeaders();
        this.setRoutes();
        this.express.listen(server.port, () => {
            console.log('Server started on port ' + server.port);
        });
    }

    setRoutes()
    {
        apiRouterService.initMiddleWares(this.express);
        apiRouterService.buildApiRoutes();
        this.express.use('/api', apiRouterService.getApiRoutes());
        this.express.use('/uploads', express.static('uploads'));
    }

    setHeaders()
    {
        this.express.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", "*");
            res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Access-Token, X-Access-Type, Content-Type, Accept");
            res.header("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, PATCH");
            next();
        });
    }
}

new AppServer().run();

